(define-module (build dotnet-build-system)
  #:use-module ((guix build gnu-build-system) #:prefix gnu:)
  #:use-module (guix build utils)
  #:use-module (ice-9 match)
  #:export (%standard-phases
            dotnet-build))

(define* (dotnet-env #:allow-other-keys)
  (let* ((directory (or (getenv "TMPDIR") "/tmp"))
         (template (string-append directory "/dotnet-fake-home.XXXXXX"))
         (home (mkdtemp template)))
    ;; Dotnet expects a writeable home directory for it's configuration files.
    (setenv "HOME" home)
    ;; Don't try to expand NuGetFallbackFolder to disk
    (setenv "DOTNET_SKIP_FIRST_TIME_EXPERIENCE" "1")
    ;; Disable the welcome message
    (setenv "DOTNET_NOLOGO" "1")
    (setenv "DOTNET_CLI_TELEMETRY_OPTOUT" "1")))

(define* (copy-nuget-inputs #:key inputs #:allow-other-keys)
  "Link nuget inputs into a central place so DOTNET can restore them."
  (define (copy-input input)
    (copy-file input (string-append "./nugets/" (strip-store-file-name input))))
  (begin
    (mkdir "nugets")
    (for-each copy-input inputs)))

(define* (restore-nuget-inputs #:key inputs #:allow-other-keys)
  "Run DOTNET restore --source ./nugets"
  (invoke "dotnet" "restore" "--source" "./nugets"))

(define* (publish #:key output (project-name #f) (nuget? #f) (self-contained? #t) (strip-debug-symbols? #t) #:allow-other-keys)
  "Run DOTNET publish or DOTNET pack"
  (define arguments `(,@(if nuget? "pack" "publish")
                      ,@(if project-name (list project-name) '())
                      "--configuration" "Release"
                      ;; don't try to access nuget.org, use local cache prepared in restore-nuget-inputs
                      "--no-restore"
                      ,@(if self-contained? '("--self-contained") '())
                      ;; TODO cross-compilation
                      "--target" "linux-x64"
                      "-p:UseAppHost=true"
                      ,@(if strip-debug-symbols? '("-p:DebugSymbols=false" "-p:DebugType=none") '())
                      "--output" output)))

(define %standard-phases
  (modify-phases gnu:%standard-phases)
  (delete 'bootstrap)
  (delete 'configure)
  (delete 'build)
  (add-before 'check dotnet-env)
  (add-before 'check copy-nuget-inputs)
  (add-before 'check publish)
  (replace 'install publish))

(define* (dotnet-build #:key inputs (phases %standard-phases)
                       #:allow-other-keys #:rest args)
  "Build the given package, applying all of PHASES in order."
  (apply gnu:gnu-build #:inputs inputs #:phases phases args))
