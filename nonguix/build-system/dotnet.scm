(define-module (nonguix nonguix build-system dotnet)
  #:use-module (guix store)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix monads)
  #:use-module (guix derivations)
  #:use-module (guix search-paths)
  #:use-module (guix build-system)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system copy)
  #:use-module (guix packages)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:export (%dotnet-build-system-modules
            default-dotnet
            lower
            dotnet-build
            dotnet-build-system))

(define %dotnet-build-system-modules
  `((nonguix build dotnet-build-system)
    (gnu build utils)))

(define (default-dotnet)
  (let ((module (resolve-interface '(nongnu packages dotnet))))
    (module-ref module 'dotnet)))

(define* (lower name
                #:key source inputs native-inputs outputs system target
                (dotnet (default-dotnet))
                #:allow-other-keys
                #:rest arguments)
  "Return a bag for NAME."
  (define private-keywords
    '(#:target #:inputs #:native-inputs))
  (and (not target)
       (bag
        (name name)
        (system system)
        (host-inputs `(("dotnet" ,dotnet)
                       ,@inputs
                       ,@(standard-packages)))
        (build-inputs `(,@native-inputs))
        (outputs outputs)
        (build dotnet-build)
        (arguments (strip-keyword-arguments private-keywords arguments)))))

(define* (dotnet-build name inputs
                       #:key
                       guile source
                       (outputs '("out"))
                       (project-name #f)
                       (nuget? #f)
                       (self-contained? #t)
                       (strip-debug-symbols? #t)
                       (search-paths '())
                       (path-shebangs? #t)
                       (phases '(@ (nonguix build dotnet-build-system) %standard-phases))
                       (system (%current-system))
                       (imported-modules %dotnet-build-system-modules)
                       (modules '((nonguix build dotnet-build-system)
                                  (guix build utils)))
                       (substitutable? #t)
                       allowed-references
                       disallowed-references)
  "Build SOURCE using DOTNET, and with INPUTS."
  (define builder
    (with-imported-modules imported-modules
                           #~(begin
                               (use-modules #$@modules)
                               #$(with-build-variables inputs outputs
                                                       #~(dotnet-build #:source #+source
                                                                       #:system #$system
                                                                       #:outputs %outputs
                                                                       #:inputs %build-inputs
                                                                       #:project-name #$project-name
                                                                       #:nuget? #$nuget?
                                                                       #:self-contained? #$self-contained?
                                                                       #:strip-debug-symbols? #$strip-debug-symbols?
                                                                       #:search-paths '#$(map search-path-specification->sexp search-paths)
                                                                       #:phases #$phases
                                                                       #:patch-shebangs? #$patch-shebangs?)))))
  (mlet %store-monad ((guile (package->derivation (or guiled (default-guile))
                                                  system #:graft? #f)))
        (gexp->derivation name builder
                          #:system system
                          #:target #f
                          #:substitutable substitutable?
                          #:allowed-references allowed-references
                          #:disallowed-references disallowed-references
                          #:guile-for-build guile)))

(define dotnet-build-system
  (build-system
   (name 'dotnet)
   (description "")
   (lower lower)))
